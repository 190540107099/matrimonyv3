import 'package:flutter/material.dart';
import 'package:matrimony/screens/AddScreen.dart';
import 'package:matrimony/screens/FavoriteScreen.dart';
import 'package:matrimony/screens/SearchScreen.dart';
import 'package:matrimony/screens/ViewScreen.dart';

import 'customui/tiles.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Matrimony',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SafeArea(
              child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.fromLTRB(0, 10, 0, 60),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          SizedBox(
                            width: 20,
                          ),
                          Text(
                            "DashBoard",
                            style: TextStyle(
                              color: Color.fromRGBO(34, 33, 91, 1),
                              fontSize: 16,
                              fontFamily: 'Gilroy-Semibold',
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: SizedBox(
                        width: double.infinity,
                        child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            elevation: 20,
                            color: const Color.fromRGBO(
                              34,
                              33,
                              91,
                              1,
                            ),
                            child: InkWell(
                              borderRadius: BorderRadius.circular(20),
                              splashColor: Colors.black54,
                              onTap: () {},
                              child: Padding(
                                  padding: const EdgeInsets.all(20),
                                  child: Stack(
                                    alignment: Alignment.topCenter,
                                    children: [
                                      Column(
                                        children: [
                                          Container(
                                            margin: const EdgeInsets.fromLTRB(
                                                0, 0, 0, 10),
                                            child: CircleAvatar(
                                              maxRadius: 25,
                                              child: Image.asset(
                                                'assets/image/profile_image.png',
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: const EdgeInsets.fromLTRB(
                                                0, 0, 0, 0),
                                            child: const Text(
                                              'Arik Kantesaria',
                                              style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.white,
                                                fontFamily: 'Gilroy-Semibold',
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: const EdgeInsets.fromLTRB(
                                                0, 5, 0, 0),
                                            child: const Text(
                                              'Flutter Developer',
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 13,
                                                fontFamily: 'Gilroy-Light',
                                                fontWeight: FontWeight.w400,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 0, 10, 0),
                                            child: Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  0, 10, 0, 0),
                                              child: const Text(
                                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ornare pretium placerat ut platea.',
                                                style: TextStyle(
                                                  fontSize: 13,
                                                  color: Color.fromRGBO(
                                                      255, 255, 255, 0.6),
                                                  fontFamily: 'Gilroy-Light',
                                                  fontWeight: FontWeight.w400,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      Container(
                                        alignment: Alignment.topRight,
                                        child: Image.asset(
                                          'assets/icon/pro.png',
                                          width: 40,
                                        ),
                                      )
                                    ],
                                  )),
                            )),
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.fromLTRB(0, 100, 0, 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                child: InkWell(
                                  borderRadius: BorderRadius.circular(20),
                                  splashColor: Color.fromRGBO(65, 94, 182, 1),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => AddScreen()),
                                    );
                                  },
                                  child: Tiles(
                                    imgAssets: 'assets/icon/add_user.png',
                                    color: Color.fromRGBO(65, 94, 182, 1),
                                    folderName: 'Add User',
                                    bgColor: Color.fromRGBO(238, 247, 254, 1),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 9.5,
                              ),
                              Expanded(
                                child: InkWell(
                                  borderRadius: BorderRadius.circular(20),
                                  splashColor: Color.fromRGBO(255, 177, 16, 1),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ViewScreen()),
                                    );
                                  },
                                  child: Tiles(
                                    imgAssets: 'assets/icon/view.png',
                                    color: Color.fromRGBO(255, 177, 16, 1),
                                    folderName: 'List',
                                    bgColor: Color.fromRGBO(255, 251, 236, 1),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.fromLTRB(0, 0, 0, 9.5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                child: InkWell(
                                  borderRadius: BorderRadius.circular(20),
                                  splashColor: Color.fromRGBO(172, 64, 64, 1),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SearchScreen()),
                                    );
                                  },
                                  child: Tiles(
                                    imgAssets: 'assets/icon/search1.png',
                                    color: Color.fromRGBO(172, 64, 64, 1),
                                    folderName: 'Search',
                                    bgColor: Color.fromRGBO(254, 238, 238, 1),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 9.5,
                              ),
                              Expanded(
                                child: InkWell(
                                    borderRadius: BorderRadius.circular(20),
                                    splashColor:
                                        Color.fromRGBO(35, 176, 176, 1),
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                FavoriteScreen()),
                                      );
                                    },
                                    child: Tiles(
                                      imgAssets: 'assets/icon/favorite.png',
                                      color: Color.fromRGBO(35, 176, 176, 1),
                                      folderName: 'Favurite',
                                      bgColor: Color.fromRGBO(240, 255, 255, 1),
                                    )),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          )),
        ],
      ),
    );
  }
}

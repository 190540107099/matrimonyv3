import 'package:flutter/material.dart';
import '../database/db_service.dart';
import '../model/userMeta.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  late List<User> nb = [];
  late int index;
  DbService dbService = DbService();
  void get() async {
    nb = await dbService.getfav();
    setState(() {});
  }

  void initState() {
    get();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(top: 30, left: 30, right: 30),
        child: ListView(
          children: [
            Column(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          size: 20,
                          color: Color.fromRGBO(34, 33, 91, 1),
                        ),
                      ),
                      Text(
                        "Favorite",
                        style: TextStyle(
                          color: Color.fromRGBO(34, 33, 91, 1),
                          fontSize: 16,
                          fontFamily: 'Gilroy-Semibold',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                    ],
                  ),
                ),
                _fetchData(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _fetchData() {
    return FutureBuilder<List<User>>(
        future: dbService.getUser(),
        builder: (BuildContext context, AsyncSnapshot<List<User>> user) {
          if (user.hasData) {
            return _buildDataTable(user.data!);
          }
          return Center(child: CircularProgressIndicator());
        });
  }

  _buildDataTable(List<User> user) {
    return Container(
      padding: EdgeInsets.only(bottom: 30),
      height: (MediaQuery.of(context).size.height),
      child: ListView.builder(
          itemCount: nb.length,
          padding: EdgeInsets.only(bottom: 100),
          itemBuilder: (
            BuildContext context,
            int index,
          ) {
            this.index = index;

            return Container(
              margin: EdgeInsets.only(bottom: 10),
              child: SizedBox(
                width: double.infinity,
                child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    elevation: 5,
                    color: const Color.fromRGBO(34, 33, 91, 1),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(20),
                      splashColor: Colors.black54,
                      onTap: () {},
                      child: Stack(
                        children: [
                          Container(
                            padding:
                                EdgeInsets.only(right: 30, top: 30, bottom: 30),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Column(
                                    children: [
                                      CircleAvatar(
                                        child: Image.asset(
                                          nb[index].toJson()["Gender"] as int ==
                                                  1
                                              ? "assets/image/Male.png"
                                              : nb[index].toJson()["Gender"]
                                                          as int ==
                                                      2
                                                  ? "assets/image/Female.png"
                                                  : "assets/image/Transgender.png",
                                          height: 60,
                                          width: 60,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 10),
                                        child: Text(
                                          nb[index].toJson()['UserName'],
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.white,
                                            fontFamily: 'Gilroy-Semibold',
                                            fontWeight: FontWeight.w700,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    height: 120,
                                    child: ListView(
                                      scrollDirection: Axis.horizontal,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  "Date of birth : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                Text(
                                                  nb[index].toJson()["DOB"],
                                                  style: TextStyle(
                                                    fontSize: 13,
                                                    color: Color.fromRGBO(
                                                        255, 255, 255, 0.6),
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "Age : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                Text(
                                                  (nb[index].toJson()["Age"])
                                                      .toString(),
                                                  style: TextStyle(
                                                    fontSize: 13,
                                                    color: Color.fromRGBO(
                                                        255, 255, 255, 0.6),
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "Gender :\t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                // Text((() {
                                                //   var check =
                                                //       nb[index].toJson()['Gender'];
                                                //   if (true) {
                                                //     return "tis true";
                                                //   }
                                                // })()),
                                                nb[index].toJson()["Gender"]
                                                            as int ==
                                                        1
                                                    ? Text(" Male",
                                                        style: TextStyle(
                                                          fontSize: 13,
                                                          color: Color.fromRGBO(
                                                              255,
                                                              255,
                                                              255,
                                                              0.6),
                                                          fontFamily:
                                                              'Gilroy-Light',
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        ))
                                                    : Text(" Female",
                                                        style: TextStyle(
                                                          fontSize: 13,
                                                          color: Color.fromRGBO(
                                                              255,
                                                              255,
                                                              255,
                                                              0.6),
                                                          fontFamily:
                                                              'Gilroy-Light',
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        )),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "Mobile Number : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                // Text((() {
                                                //   var check =
                                                //       nb[index].toJson()['Gender'];
                                                //   if (true) {
                                                //     return "tis true";
                                                //   }
                                                // })()),
                                                Text(
                                                    nb[index].toJson()[
                                                        'MobileNumber'],
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          255, 255, 255, 0.6),
                                                      fontFamily:
                                                          'Gilroy-Light',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ))
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "Email : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                // Text((() {
                                                //   var check =
                                                //       nb[index].toJson()['Gender'];
                                                //   if (true) {
                                                //     return "tis true";
                                                //   }
                                                // })()),
                                                Text(
                                                    nb[index].toJson()['Email'],
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          255, 255, 255, 0.6),
                                                      fontFamily:
                                                          'Gilroy-Light',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ))
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "City : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                // Text((() {
                                                //   var check =
                                                //       nb[index].toJson()['Gender'];
                                                //   if (true) {
                                                //     return "tis true";
                                                //   }
                                                // })()),
                                                Text(
                                                    nb[index]
                                                        .toJson()['CityName'],
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          255, 255, 255, 0.6),
                                                      fontFamily:
                                                          'Gilroy-Light',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ))
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "State : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                Text(
                                                    nb[index]
                                                        .toJson()['StateName'],
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          255, 255, 255, 0.6),
                                                      fontFamily:
                                                          'Gilroy-Light',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ))
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "Country : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                Text(
                                                    nb[index].toJson()[
                                                        'CountryName'],
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          255, 255, 255, 0.6),
                                                      fontFamily:
                                                          'Gilroy-Light',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ))
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )),
              ),
            );
          }),
    );
  }
}

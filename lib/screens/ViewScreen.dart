import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:matrimony/database/db_service.dart';
import 'package:matrimony/main.dart';
import 'package:matrimony/screens/AddScreen.dart';

import '../model/userMeta.dart';

class ViewScreen extends StatefulWidget {
  const ViewScreen({Key? key}) : super(key: key);

  @override
  State<ViewScreen> createState() => _ViewScreenState();
}

class _ViewScreenState extends State<ViewScreen> {
  late List<User> nb = [];
  late int index;
  void get() async {
    nb = await dbService.getUser();
    setState(() {});
  }

  void initState() {
    get();
  }

  DbService dbService = DbService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(top: 30, left: 30, right: 30),
        child: ListView(
          children: [
            Column(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          size: 20,
                          color: Color.fromRGBO(34, 33, 91, 1),
                        ),
                      ),
                      Text(
                        "View Details",
                        style: TextStyle(
                          color: Color.fromRGBO(34, 33, 91, 1),
                          fontSize: 16,
                          fontFamily: 'Gilroy-Semibold',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                    ],
                  ),
                ),
                _fetchData(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _fetchData() {
    return FutureBuilder<List<User>>(
        future: dbService.getUser(),
        builder: (BuildContext context, AsyncSnapshot<List<User>> user) {
          if (user.hasData) {
            return _buildDataTable(user.data!);
          }
          return Center(child: CircularProgressIndicator());
        });
  }

  _buildDataTable(List<User> user) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      height: (MediaQuery.of(context).size.height),
      child: ListView.builder(
          itemCount: nb.length,
          padding: EdgeInsets.only(bottom: 100),
          itemBuilder: (
            BuildContext context,
            int index,
          ) {
            this.index = index;

            return Container(
              margin: EdgeInsets.only(bottom: 10),
              child: SizedBox(
                width: double.infinity,
                child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    elevation: 5,
                    color: const Color.fromRGBO(34, 33, 91, 1),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(20),
                      splashColor: Colors.black54,
                      onTap: () {},
                      child: Stack(
                        children: [
                          Container(
                            padding: EdgeInsets.only(right: 10, top: 10),
                            child: Container(
                              alignment: Alignment.topRight,
                              child: FavoriteButton(
                                isFavorite:
                                    nb[index].IsFavorite == 1 ? true : false,
                                valueChanged: (favbutton) {
                                  if (favbutton == true) {
                                    dbService.setFav(
                                        1, int.parse(nb[index].id.toString()));
                                  }
                                  if (favbutton == false) {
                                    dbService.setFav(
                                        0, int.parse(nb[index].id.toString()));
                                  }
                                },
                              ),
                            ),
                          ),
                          Container(
                            padding:
                                EdgeInsets.only(right: 30, top: 30, bottom: 30),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Column(
                                    children: [
                                      Image.asset(
                                        nb[index].toJson()["Gender"] as int == 1
                                            ? "assets/image/Male.png"
                                            : nb[index].toJson()["Gender"]
                                                        as int ==
                                                    2
                                                ? "assets/image/Female.png"
                                                : "assets/image/Transgender.png",
                                        height: 60,
                                        width: 60,
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 10),
                                        child: Text(
                                          nb[index].toJson()['UserName'],
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.white,
                                            fontFamily: 'Gilroy-Semibold',
                                            fontWeight: FontWeight.w700,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Container(
                                        child: Container(
                                          margin: EdgeInsets.only(top: 20),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              AddScreen(
                                                                isEditMode:
                                                                    true,
                                                                user:
                                                                    user[index],
                                                              )));
                                                },
                                                child: Icon(
                                                  Icons.edit,
                                                  color: Colors.green,
                                                  size: 30,
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                          contex) {
                                                        return AlertDialog(
                                                          title: Text("Delete"),
                                                          content: Text(
                                                              "Do you want to delete this record"),
                                                          actions: [
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                ElevatedButton(
                                                                  onPressed:
                                                                      () {
                                                                    dbService
                                                                        .deleteUser(
                                                                            nb[index])
                                                                        .then(
                                                                      (value) {
                                                                        setState(
                                                                            () {
                                                                          Navigator.push(
                                                                              context,
                                                                              MaterialPageRoute(builder: (context) => MyHomePage()));
                                                                        });
                                                                      },
                                                                    );
                                                                  },
                                                                  child: Text(
                                                                      "Delete"),
                                                                ),
                                                                SizedBox(
                                                                  width: 5,
                                                                ),
                                                                ElevatedButton(
                                                                  onPressed:
                                                                      () {
                                                                    Navigator.of(
                                                                            context)
                                                                        .pop();
                                                                  },
                                                                  child: Text(
                                                                      "No"),
                                                                )
                                                              ],
                                                            )
                                                          ],
                                                        );
                                                      });
                                                },
                                                child: Icon(
                                                  Icons.delete_rounded,
                                                  color: Colors.red,
                                                  size: 30,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    height: 120,
                                    child: ListView(
                                      scrollDirection: Axis.horizontal,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  "Date of birth : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                Text(
                                                  nb[index].toJson()["DOB"],
                                                  style: TextStyle(
                                                    fontSize: 13,
                                                    color: Color.fromRGBO(
                                                        255, 255, 255, 0.6),
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "Age : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                Text(
                                                  (nb[index].toJson()["Age"])
                                                      .toString(),
                                                  style: TextStyle(
                                                    fontSize: 13,
                                                    color: Color.fromRGBO(
                                                        255, 255, 255, 0.6),
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "Gender :\t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                // Text((() {
                                                //   var check =
                                                //       nb[index].toJson()['Gender'];
                                                //   if (true) {
                                                //     return "tis true";
                                                //   }
                                                // })()),
                                                nb[index].toJson()["Gender"]
                                                            as int ==
                                                        1
                                                    ? const Text(
                                                        " Male",
                                                        style: TextStyle(
                                                            color: Color(
                                                                0xFFB2A9A9),
                                                            fontFamily:
                                                                'Montserrat',
                                                            fontSize: 12.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500),
                                                      )
                                                    : nb[index].toJson()[
                                                                    "Gender"]
                                                                as int ==
                                                            2
                                                        ? const Text(
                                                            " Female",
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xFFB2A9A9),
                                                              fontFamily:
                                                                  'Montserrat',
                                                              fontSize: 12.0,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                            ),
                                                          )
                                                        : const Text(
                                                            " Other",
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xFFB2A9A9),
                                                              fontFamily:
                                                                  'Montserrat',
                                                              fontSize: 12.0,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                            ),
                                                          ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "Mobile Number : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                // Text((() {
                                                //   var check =
                                                //       nb[index].toJson()['Gender'];
                                                //   if (true) {
                                                //     return "tis true";
                                                //   }
                                                // })()),
                                                Text(
                                                    nb[index].toJson()[
                                                        'MobileNumber'],
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          255, 255, 255, 0.6),
                                                      fontFamily:
                                                          'Gilroy-Light',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ))
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "Email : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                // Text((() {
                                                //   var check =
                                                //       nb[index].toJson()['Gender'];
                                                //   if (true) {
                                                //     return "tis true";
                                                //   }
                                                // })()),
                                                Text(
                                                    nb[index].toJson()['Email'],
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          255, 255, 255, 0.6),
                                                      fontFamily:
                                                          'Gilroy-Light',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ))
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "City : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                // Text((() {
                                                //   var check =
                                                //       nb[index].toJson()['Gender'];
                                                //   if (true) {
                                                //     return "tis true";
                                                //   }
                                                // })()),
                                                Text(
                                                    nb[index]
                                                        .toJson()['CityName'],
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          255, 255, 255, 0.6),
                                                      fontFamily:
                                                          'Gilroy-Light',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ))
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "State : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                Text(
                                                    nb[index]
                                                        .toJson()['StateName'],
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          255, 255, 255, 0.6),
                                                      fontFamily:
                                                          'Gilroy-Light',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ))
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "Country : \t",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontFamily: 'Gilroy-Light',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                                Text(
                                                    nb[index].toJson()[
                                                        'CountryName'],
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      color: Color.fromRGBO(
                                                          255, 255, 255, 0.6),
                                                      fontFamily:
                                                          'Gilroy-Light',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ))
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )),
              ),
            );
          }),
    );
  }
}
